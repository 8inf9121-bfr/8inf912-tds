# 8INF912 - Sujet spécial en informatique II - Programmation réseau dans les jeux vidéo

__Auteurs :__

- Antoine BOULANGE - BOUA08049704
- Aymeric FERRON - FERA19039907
- Léonard RIZZO - RIZA24029900

## TP1 - Sockets Berkeley 

Réalisation d'une bibliothèque manipulant des sockets Berkeley et application dans le cadre d'un chat.

__Question : Quel protocole choisi-t-on pour le chat du jeu vidéo ? UDP ou TCP et pourquoi ?__

TCP semble être le protocole le plus adapté dans le cadre d'un chat dans un jeu vidéo. En effet, TCP permet de gérer l'ordonancement et les erreurs, des éléments indispensables dans le cadre d'une communication entre deux individus. Si le contenu de la trame est désordonnancé ou si une partie de ses données est perdue, les messages deviennent aussitôt intintéligible par les joueurs qui les reçoivent. D'autre part, dans ce genre de contexte, il est courant que l'interface prévienne le joueur si son message a bien été distribué ou si une erreur est survenue, ce que TCP gère, mais pas UDP.  

__Travail effectué:__

- Partie 1: La librairie contient:
    - Les classes Network (classe mère), Terminal et Connection.
    - UDPConnection et TCPConnection héritent de la classe connection, mais seule la classe TCPConnection est complète, la seconde permet seulement une connection entre un serveur et un seul client.
    - La gestion des thread pour le client et le server (seulement en TCP).
    - La déconnexion n'est pas totalement implémentée (ctrl+C sur un client provoque une erreur).
- Partie 2:
    - les exécutables sont complets, le client se connecte par défaut à l'adresse 127.0.0.1 si aucun argument n'est passé en entrée

Le projet utilise CMake et les exécutables sont générés aux chemins suivants :
- `out\build\x64-Debug\Chat\Client\Client.exe` pour le client
- `out\build\x64-Debug\Chat\Server\Server.exe` pour le serveur
