#include <WinSock2.h>
#include <iostream>
#include "UDPConnection.hpp"

namespace uqac::network {
	UDPConnection::UDPConnection() {
		m_socket = INVALID_SOCKET;
	}

	void UDPConnection::setSocket(SOCKET sock) {
		m_socket = sock;
	}

	int UDPConnection::sendMessage(char* message, int messageSize, std::function<void(char*, int, int)> callback) {
		std::cout << "Sending message in socket " << m_socket << std::endl;
		struct sockaddr_in si_other;
		int slen = sizeof(si_other);
		char buffer[DEFAULT_BUFLEN];

		//setup address structure
		memset((char*)&si_other, 0, sizeof(si_other));
		si_other.sin_family = AF_INET;
		si_other.sin_port = htons(atoi(DEFAULT_PORT));
		si_other.sin_addr.S_un.S_addr = inet_addr(DEFAULT_ADDR);

		//printf("Enter message : ");
		//std::cin.getline(message, DEFAULT_BUFLEN);

		//send the message
		if (sendto(m_socket, message, (int) strlen(message), 0, (struct sockaddr*)&si_other, slen) == SOCKET_ERROR)
		{
			printf("sendto() failed with error code : %d", WSAGetLastError());
			exit(EXIT_FAILURE);
		}

		int iResult = 0;

		//receive a reply and print it
		//clear the buffer by filling null, it might have previously received data
		memset(buffer, '\0', DEFAULT_BUFLEN);
		//try to receive some data, this is a blocking call
		//if (iResult = recvfrom(m_socket, buffer, DEFAULT_BUFLEN, 0, (struct sockaddr*)&si_other, &slen) == SOCKET_ERROR)
		//{
		//	printf("recvfrom() failed with error code : %d", WSAGetLastError());
		//	exit(EXIT_FAILURE);
		//}

		std::cout << buffer << std::endl;


		//callback(message, messageSize, iResult);
		return iResult;
	}

	int UDPConnection::broadcast(char* message, int messageSize, SOCKET sourceSock, std::function<void(char*, int, int)> callback) {
		std::cout << "Broadcast Udp. Doesn't work" << std::endl;
		return EXIT_SUCCESS;
	}


	/*
	No callback is called yet
	*/
	int UDPConnection::receiveMessage(std::function<void(char*, int, int)> callback) {
		if (m_socket == INVALID_SOCKET) {
			return EXIT_FAILURE;
		}

		char buf[DEFAULT_BUFLEN];
		struct sockaddr_in server, si_other;
		int slen, recv_len;
		slen = sizeof(si_other);

		printf("Waiting for data...");
		fflush(stdout);

		//clear the buffer by filling null, it might have previously received data
		memset(buf, '\0', DEFAULT_BUFLEN);

		//try to receive some data, this is a blocking call
		if ((recv_len = recvfrom(m_socket, buf, DEFAULT_BUFLEN, 0, (struct sockaddr*)&si_other, &slen)) == SOCKET_ERROR)
		{
			printf("recvfrom failed with error code : %d", WSAGetLastError());
			exit(EXIT_FAILURE);
		}

		//print details of the client/peer and the data received
		std::cout << "Received packet from " << inet_ntoa(si_other.sin_addr) << ":" << ntohs(si_other.sin_port) << std::endl;
		std::cout << "Data: " << buf << std::endl;

		return EXIT_SUCCESS;
	}

	int UDPConnection::shutdownConnect(SOCKET soc) {
		return EXIT_SUCCESS;
	}

	int UDPConnection::shutdownAllConnections() {
		return EXIT_SUCCESS;
	}
}