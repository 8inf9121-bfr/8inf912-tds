#include <iostream>
#include <format>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <thread>
#include <stdio.h>
#include "Network.hpp"
#include "TCPConnection.hpp"
#include "UDPConnection.hpp"

#pragma comment(lib, "Ws2_32.lib")

namespace uqac::network {

	Network::Network(char* protocol) :	clientThread{}, 
										listenSocket{ INVALID_SOCKET },
										terminal{ nullptr }, 
										networkProtocol{protocol} {
		if (!strcmp(protocol, STR_TCP_PROTOCOL)) {
			myConnection = new TCPConnection();
		}
		else {
			myConnection = new UDPConnection();
		}
		
	}

	/* function creates a client */
	void Network::createClient(std::function<void(char*, int, int)> onClientCreatedCallback, char* ip, char* port) {
		initWinsock();

		if (!strcmp(this->networkProtocol, STR_TCP_PROTOCOL)) {
			initTcpClient(ip, port, onClientCreatedCallback);
		}
		else {
			initUdpClient(ip, port);
		}
	}

	/* function creates a server */
	void Network::createServer(std::function<void(char*, int, int)> onServerCreatedCallback, char* port) {
		initWinsock();

		if (!strcmp(this->networkProtocol, STR_TCP_PROTOCOL)) {
			initTcpServer(port, onServerCreatedCallback);
		}
		else {
			initUdpServer(port, onServerCreatedCallback);
		}
	}

	/* function shuts down the server */
	void Network::shutdownServer() {
		std::cout << "[Network] Shutting down the server" << std::endl;
		int iResult = myConnection->shutdownAllConnections();
		if (iResult == EXIT_FAILURE) {
			std::cout << "[ERR] Shutdown failed in Network::shutdownServer() : " + WSAGetLastError() << std::endl;
			WSACleanup();
			return;
		}

		terminal->closeThread();

		// Cleanup
		free(myConnection);
		closesocket(listenSocket);
		WSACleanup();
	}

	/* function initializes Winsock (from Windows tutorial) */
	int Network::initWinsock() {
		WSADATA wsaData;
		int iResult = 0;
		iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0) {
			std::cout << "WSAStartup failed" << std::endl;
			return 1;
		}
		return 0;
	}

	/* function initializes TCP server (inspired by Windows tutorial) */
	int Network::initTcpServer(char* port, std::function<void(char*, int, int)> onInitTcpServerCallback) {
		int iResult = 0;

		// Creates addrinfo
		struct addrinfo* result = NULL, * ptr = NULL, hints;

		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;
		hints.ai_flags = AI_PASSIVE;

		// Resolves the local address and port to be used by the server
		iResult = getaddrinfo(NULL, port, &hints, &result);
		if (iResult != 0) {
			std::cout << "getaddrinfo failed" << iResult << std::endl;
			WSACleanup();
			return 1;
		}

		// Creates the socket for the server to listen for client connections
		listenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

		if (listenSocket == INVALID_SOCKET) {
			std::cout << "[ERR] Error when creating the socket in Network::initTcpServer() : " << WSAGetLastError() << std::endl;
			freeaddrinfo(result);
			WSACleanup();
			return 1;
		}

		// Sets up the TCP listening socket (bind)
		iResult = bind(listenSocket, result->ai_addr, (int)result->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			std::cout << "[ERR] bind() failed in Network::initTcpServer() : " << WSAGetLastError() << std::endl;
			freeaddrinfo(result);
			closesocket(listenSocket);
			WSACleanup();
			return 1;
		}

		freeaddrinfo(result);

		// SOMAXCONN instructs the Winsock provider for this socket to allow a maximum reasonable number of pending connections in the queue
		if (listen(listenSocket, SOMAXCONN) == SOCKET_ERROR) { 
			std::cout << "[ERR] listen() failed in Network::initTcpServer() : " << WSAGetLastError() << std::endl;
			closesocket(listenSocket);
			WSACleanup();
			return 1;
		}

		// Creates a Terminal to manage new clients
		terminal = new Terminal(listenSocket);
		terminal->listenTcp((TCPConnection*)myConnection, onInitTcpServerCallback);

		std::cout << std::endl << "[Network] Started server in TCP, listening on socket: "<< listenSocket << std::endl << std::endl;
		return 9;
	}

	/* function initializes UDP server (inspired by Windows tutorial) */
	int Network::initUdpServer(char* port, std::function<void(char*, int, int)> onInitUdpServerCallback) {
		struct sockaddr_in server;

		// Creates a socket
		if ((listenSocket = socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET)
		{
			std::cout << "[ERR] Error when creating the socket in Network::initUdpServer() : " << WSAGetLastError() << std::endl;
		}

		// Prepares the sockaddr_in structure
		server.sin_family = AF_INET;
		server.sin_addr.s_addr = INADDR_ANY;
		server.sin_port = htons(atoi(DEFAULT_PORT));

		// Binds
		if (bind(listenSocket, (struct sockaddr*)&server, sizeof(server)) == SOCKET_ERROR)
		{
			std::cout << "[ERR] bind() failed in Network::initUdpServer() : " << WSAGetLastError() << std::endl;
			exit(EXIT_FAILURE);
		}


		// Creates a Terminal to manage new clients
		terminal = new Terminal(listenSocket);
		terminal->listenUdp((UDPConnection*)myConnection, onInitUdpServerCallback);

		return EXIT_SUCCESS;
	}

	/* function initializes TCP client */
	int Network::initTcpClient(char* ip, char* port, std::function<void(char*, int, int)> onInitTcpClientCallback) {
		int iResult = 0;
		struct addrinfo* result = NULL, * ptr = NULL, hints;

		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;

		// Resolves the server address and port, using the server adress given on the command line
		iResult = getaddrinfo(ip, DEFAULT_PORT, &hints, &result);
		if (iResult != 0) {
			std::cout << "[ERR] getaddrinfo() failed in Network::initTcpClient() : " + iResult << std::endl;
			WSACleanup();
			return 1;
		}

		// Creates the socket
		SOCKET clientSocket = INVALID_SOCKET;

		// Attempt to connect to an address until one succeeds
		for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

			// Creates a SOCKET to connect to the server
			clientSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

			if (clientSocket == INVALID_SOCKET) {
				std::cout << "[ERR] Error when creating the socket in Network::initTcpClient() : " + WSAGetLastError() << std::endl;
				freeaddrinfo(result);
				WSACleanup();
				return 1;
			}
			else {
				myConnection->setSocket(clientSocket);
			}

			// Connects to the server
			iResult = connect(clientSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (iResult == SOCKET_ERROR) {
				std::cout << "[ERR] connect() failed in Network::initTcpClient() : " + WSAGetLastError() << std::endl;
				closesocket(clientSocket);
				clientSocket = INVALID_SOCKET;
				continue;
			}
			break;
		}

		if (clientSocket == INVALID_SOCKET) {
			std::cout << "[ERR] Unable to connect to server" << std::endl;
			WSACleanup();
			return 1;
		}

		freeaddrinfo(result);

		clientThread = new std::thread([=](
			std::function<void(char*, int, int)> callback) {
				listenConnection(callback);
			},
			onInitTcpClientCallback
		);

		return 0;
	}
	

	/* function initializes UDP client */
	int Network::initUdpClient(char* ip, char* port) {
		SOCKET tmpSocket;

		if ((tmpSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR)
		{
			std::cout << "[ERR] Error when creating the socket in Network::initUdpClient() : " << WSAGetLastError() << std::endl;
			exit(EXIT_FAILURE);
		}
		this->myConnection->setSocket(tmpSocket);
	}

	/* function disconnects (for client) */
	int Network::disconnect() {
		int iResult = 0;
		iResult = myConnection->shutdownAllConnections();
		if (iResult == SOCKET_ERROR) {
			std::cout << "shutdown failed " << WSAGetLastError() << std::endl;
			WSACleanup();
			return 1;
		}

		clientThread->join();

		// Cleanup
		free(myConnection);
		WSACleanup();

		return 0;
	}

	/* function returns true if the parameter is the exit command */
	bool Network::isExitCmd(char* cmd) {
		return !strcmp(cmd, EXIT_CMD);
	}

	/* function listens for new connections */
	void Network::listenConnection(std::function<void(char*, int, int)> callback) {
		int iResult = 1;
		while (iResult > 0) {
			iResult = this->myConnection->receiveMessage(callback);
		}
	}
}