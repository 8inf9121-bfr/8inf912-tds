#include <WinSock2.h>
#include <iostream>
#include "TCPConnection.hpp"

namespace uqac::network {
	TCPConnection::TCPConnection() {
		m_socket = INVALID_SOCKET;
		nextConnection = nullptr;
	}

	void TCPConnection::setSocket(SOCKET sock) {
		if (m_socket == INVALID_SOCKET) {
			m_socket = sock;
			return;
		}
		else if (nextConnection == nullptr) {
			nextConnection = new TCPConnection();
		}
		nextConnection->setSocket(sock);
	}

	/*
	Callback arguments: 
	-char* is the message sent
	-int is the size of the message sent
	-int is the int returned by winsock send() function

	function returns the int returned by winsock send() function
	*/
	int TCPConnection::sendMessage(char* message, int messageSize, std::function<void(char*, int, int)> onSendCallback) {
		int iResult = 0;
		
		// Send an initial buffer
		iResult = send(this->m_socket, message, messageSize, 0);
		if (iResult == SOCKET_ERROR) {
			std::cout << "[ERR] send() failed in TCPConnection::sendMessage() : " << WSAGetLastError() << std::endl;
			closesocket(this->m_socket);
			WSACleanup();
			return iResult;
		}

		onSendCallback(message, messageSize, iResult);
		return iResult;
	}

	/*
	Callback arguments:
	- char* and int ar the same as sendMessage
	- int is the socket source of the broadcast

	function transfer a message to all other connected users
	*/
	int TCPConnection::broadcast(char* message, int messageSize, SOCKET sourceSock, std::function<void(char*, int, int)> onBroadcastCallback) {
		if (this->m_socket != INVALID_SOCKET) {
			if (this->m_socket != sourceSock) {
				this->sendMessage(message, messageSize, onBroadcastCallback);
			}
		}
		if (this->nextConnection != nullptr) {
			this->nextConnection->broadcast(message, messageSize, sourceSock, onBroadcastCallback);
		}
		return EXIT_SUCCESS;
	}

	/*
	Callback arguments:
	-char* is the buffer received
	-int is the size of the buffer
	-int is the source socket

	function returns the size of the buffer received
	*/
	int TCPConnection::receiveMessage(std::function<void(char*, int, int)> onReceivedCallback) {
		if (this->m_socket == INVALID_SOCKET) {
			return EXIT_FAILURE;
		}

		char recvbuf[DEFAULT_BUFLEN];
		int recvbuflen = DEFAULT_BUFLEN;
		int iResult = 0;

		iResult = recv(this->m_socket, recvbuf, recvbuflen, 0);
		if (iResult == SOCKET_ERROR)  {
			std::cout << "[ERR] recv() failed in TCPConnection::receiveMessage() : " + WSAGetLastError() << std::endl;
			closesocket(this->m_socket);
			WSACleanup();
			return iResult;
		}

		if (iResult > 0){
			std::cout << "[TCPConnection] Receiving a message on socket: " << this->m_socket << " of size " << iResult << std::endl;
		}
			
		if (iResult == 0) {
			std::cout << "[TCPConnection] Shutting down the connection." << std::endl;
			shutdownConnect(m_socket);
		} else {
			onReceivedCallback(recvbuf, iResult, this->m_socket);
		}

		return iResult;
	}

	/*
		function 
		send operations for existing connections
		- Client : shuts down its connection
		- Server : shuts down all connections
	*/
	int TCPConnection::shutdownAllConnections() {
		// Shutdown all connections recursively
		if (nextConnection != nullptr) {
			nextConnection->shutdownAllConnections();
		}

		int iResult = shutdown(this->m_socket, SD_SEND);
		if (iResult == SOCKET_ERROR) {
			std::cout << "[ERR] shutdown() failed in TCPConnection::shutdownAllConnections() : " + WSAGetLastError() << std::endl;
			closesocket(this->m_socket);
			WSACleanup();
			return EXIT_FAILURE;
		}

		// Cleanup
		std::cout << "[TCPConnection] Closing socket " << this->m_socket << std::endl;
		closesocket(this->m_socket);
		this->m_socket = INVALID_SOCKET;
		return EXIT_SUCCESS;
	}

	/* function shuts down send operations for the specified socket */
	int TCPConnection::shutdownConnect(SOCKET soc) {
		if (this->m_socket = soc) {
			int iResult = shutdown(this->m_socket, SD_SEND);
			if (iResult == SOCKET_ERROR) {
				std::cout << "[ERR] shutdown() failed in TCPConnection::shutdownConnect() : " + WSAGetLastError() << std::endl;
				closesocket(this->m_socket);
				WSACleanup();
				return EXIT_FAILURE;
			}

			// Cleanup
			std::cout << "[TCPConnection] Closing socket " << this->m_socket << std::endl;
			closesocket(this->m_socket);
			this->m_socket = INVALID_SOCKET;
			return EXIT_SUCCESS;
		}
		else {
			if (nextConnection != nullptr) {
				nextConnection->shutdownConnect(soc);
			}
		}
	}
}