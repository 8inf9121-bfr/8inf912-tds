#include <iostream>
#include <WinSock2.h>
#include <ws2tcpip.h>

#include "Terminal.hpp"

namespace uqac::network {

	Terminal::Terminal(SOCKET sock) : listenThread{}, listeningSocket{sock} { }

	/*
		function
		- is called by server
		- listens for new clients
		- creates new socket for each new client
	*/
	void Terminal::listenTcp(TCPConnection* myConnections, std::function<void(char*, int, int)> onNewClientCallback) {
		// Launchs listenCore for TCP in a new thread
		listenThread = new std::thread([=](
			std::function<void(char*, int, int)> onNewClientLambdaCallback) {
				listenCoreTcp(myConnections, onNewClientLambdaCallback);
			},
			onNewClientCallback
		);
	}

	/*
		function
		- is called by server
		- listens for new clients
		- creates new socket for each new client
	*/
	void Terminal::listenUdp(UDPConnection* myConnections, std::function<void(char*, int, int)> onNewClientCallback) {
		// Launchs listenCore for UDP in a new thread
		listenThread = new std::thread([=](
			std::function<void(char*, int, int)> onNewClientLambdaCallback) {
				listenCoreUdp(myConnections, onNewClientLambdaCallback);
			},
			onNewClientCallback
		);
	}

	/* function listens endlessly for new clients in TCP */
	void Terminal::listenCoreTcp(TCPConnection* myConnections, std::function<void(char*, int, int)> onMessageReceived) {
		for (;;) {
			fd_set clientSockets;
			FD_ZERO(&clientSockets);

			Connection* tempConnection = myConnections;

			while (tempConnection != nullptr) {
				SOCKET sock = tempConnection->getSocket();
				if (sock != INVALID_SOCKET) {
					FD_SET(sock, &clientSockets);
				}
				tempConnection = tempConnection->getNextConnection();
			}

			FD_SET(listeningSocket, &clientSockets);

			int receivedStatus = select(0, &clientSockets, nullptr, nullptr, nullptr);

			if (receivedStatus > 0) {
				for (int i = 0; i < receivedStatus; i++) {
					SOCKET sock = clientSockets.fd_array[i];
					if (FD_ISSET(sock, &clientSockets)) {
						if (sock == listeningSocket) {
							// Accept the new client
							SOCKET clientSocket = INVALID_SOCKET;
							clientSocket = accept(this->listeningSocket, NULL, NULL);
							if (clientSocket == INVALID_SOCKET) {
								std::cout << "[Terminal] accept() failed in Terminal::listenCoreTcp() : " << WSAGetLastError() << std::endl;
								closesocket(this->listeningSocket);
								WSACleanup();
							}
							else {
								myConnections->setSocket(clientSocket);
								std::cout << "[Terminal] Client accepted, with socket: " << clientSocket << std::endl;
							}
						}
						else {
							Connection* currentConnect = myConnections->getConnection(sock);
							currentConnect->receiveMessage(onMessageReceived);
						}
					}
				}
			}
			else if (receivedStatus == 0) {
				std::cout << "[ERR] receivedStatus have an incompatible value in Terminal::listenCoreTcp()" << std::endl;
			}
			else {
				std::cout << "[ERR] Error encountered during select in Terminal::listenCoreTcp() : " << WSAGetLastError() <<std::endl;
			}
		}
	}

	/* function listens endlessly for new clients in UDP */
	void Terminal::listenCoreUdp(UDPConnection* myConnections, std::function<void(char*, int, int)> onMessageReceived) {
		struct sockaddr_in clients;
		int clientsLength = sizeof(clients);

		for (;;) {
			char buf[DEFAULT_BUFLEN];
			struct sockaddr_in server, si_other;
			int slen, recv_len;
			slen = sizeof(si_other);

			// Clears the buffer by filling null, it might have previously received data
			memset(buf, '\0', DEFAULT_BUFLEN);

			// Tries to receive some data, this is a blocking call
			if ((recv_len = recvfrom(listeningSocket, buf, DEFAULT_BUFLEN, 0, (struct sockaddr*)&si_other, &slen)) == SOCKET_ERROR)
			{
				std::cout << "[ERR] recvfrom() failed in Terminal::listenCoreUdp() : " << WSAGetLastError() << std::endl;
				exit(EXIT_FAILURE);
			}

			// Replies the client with the same data
			if (sendto(listeningSocket, buf, recv_len, 0, (struct sockaddr*)&si_other, slen) == SOCKET_ERROR)
			{
				std::cout << "[ERR] sendto() failed in Terminal::listenCoreUdp() : " << WSAGetLastError() << std::endl;
				exit(EXIT_FAILURE);
			}
		}
	}

	/* Joins the listening thread */
	void Terminal::closeThread() {
		listenThread->join();
	}
}