#pragma once
#include <WinSock2.h>
#include <thread>
#include "Defines.hpp"
#include "Terminal.hpp"
#include "Connection.hpp"

namespace uqac::network {

	class Network
	{
	public:
		Connection *myConnection;

	private:
		SOCKET listenSocket;
		Terminal* terminal;
		char* networkProtocol;
		std::thread* clientThread;

	public:
		Network() = default;
		Network(char*);
		~Network() = default;

		void createClient(std::function<void(char*, int, int)>, char* = DEFAULT_ADDR, char* = DEFAULT_PORT); // connect
		void createServer(std::function<void(char*, int, int)>, char* = DEFAULT_PORT); // listen

		void shutdownServer();

		bool isExitCmd(char*);
		int disconnect();

	private:
		int initWinsock();
		int initTcpServer(char*, std::function<void(char*, int, int)>);
		int initTcpClient(char*, char*, std::function<void(char*, int, int)>);
		int initUdpServer(char*, std::function<void(char*, int, int)>);
		int initUdpClient(char*, char*);
		
		void listenTerminal(Terminal*);
		void listenConnection(std::function<void(char*, int, int)>);
	};
}
