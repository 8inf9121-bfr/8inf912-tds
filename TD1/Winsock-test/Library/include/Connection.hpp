#pragma once

#include <WinSock2.h>
#include <functional>
#include "Defines.hpp"

namespace uqac::network {
	class Connection {
	protected:
		SOCKET m_socket;
		Connection* nextConnection; // Linked-list

	public:
		Connection() = default;
		~Connection() = default;

		virtual int sendMessage(char*, int, std::function<void(char*,int, int)>) = 0;
		virtual int broadcast(char*, int, SOCKET, std::function<void(char*, int, int)>) = 0;
		virtual int receiveMessage(std::function<void(char*, int, int)>) = 0;
		virtual int shutdownConnect(SOCKET) = 0;
		virtual int shutdownAllConnections() = 0;
		virtual bool isTcp() = 0;

		/* SETTERS */
		virtual void setSocket(SOCKET newSocket) = 0;

		/* GETTERS */
		inline SOCKET getSocket() {
			return m_socket;
		};

		inline Connection* getNextConnection() {
			return nextConnection;
		}

		inline Connection* getConnection(SOCKET sock) {
			Connection* temp = this;
			if (this->m_socket == sock) {
				return this;
			}
			else
				return this->nextConnection->getConnection(sock);
		}
	};
}