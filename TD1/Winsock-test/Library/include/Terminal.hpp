#pragma once

#include <functional>
#include <WinSock2.h>
#include <thread>
#include "TCPConnection.hpp"
#include "UDPConnection.hpp"
#include "Defines.hpp"

namespace uqac::network {
    class Terminal {

    private:
        SOCKET listeningSocket;
        std::thread* listenThread;

        void listenCoreTcp(TCPConnection*, std::function<void(char*, int, int)>);
        void listenCoreUdp(UDPConnection*, std::function<void(char*, int, int)>);

    public:
        /*
            @param SOCKET needs to be initialized
        */
        Terminal(SOCKET);
        ~Terminal() = default;

        void listenTcp(TCPConnection*, std::function<void(char*, int, int)>);
        void listenUdp(UDPConnection*, std::function<void(char*, int, int)>);
        void closeThread();
    };
}