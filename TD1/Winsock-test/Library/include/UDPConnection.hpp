#pragma once

#include "Connection.hpp"

namespace uqac::network {
	class UDPConnection : public Connection {
	public:
		UDPConnection();
		~UDPConnection() = default;

		int sendMessage(char*, int, std::function<void(char*, int, int)>);
		int broadcast(char*, int, SOCKET, std::function<void(char*, int, int)>);
		int receiveMessage(std::function<void(char*, int, int)>);
		int shutdownConnect(SOCKET);
		int shutdownAllConnections();

		inline bool isTcp() { return false; };

		/* SETTERS */
		void setSocket(SOCKET);
	};
}