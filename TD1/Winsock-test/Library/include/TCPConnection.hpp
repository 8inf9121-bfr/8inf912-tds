#pragma once

#include "Connection.hpp"

namespace uqac::network {
	class TCPConnection : public Connection {
	public:
		TCPConnection();
		~TCPConnection() = default;

		int sendMessage(char*, int, std::function<void(char*, int, int)>);
		int broadcast(char*, int, SOCKET, std::function<void(char*, int, int)>);
		int receiveMessage(std::function<void(char*, int, int)>);
		int shutdownConnect(SOCKET);
		int shutdownAllConnections();

		inline bool isTcp() { return true; };

		/* SETTERS */
		void setSocket(SOCKET);
	};
}