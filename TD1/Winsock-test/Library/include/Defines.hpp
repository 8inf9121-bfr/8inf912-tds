#pragma once

#define STR_UDP_PROTOCOL "UDP"
#define STR_TCP_PROTOCOL "TCP"
#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"
#define DEFAULT_PORT_INT 27015
#define DEFAULT_ADDR "127.0.0.1"
#define EXIT_CMD "quit"