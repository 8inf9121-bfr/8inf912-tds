#define WIN32_LEAN_AND_MEAN

#include <iostream>
#include <format>

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

#include "../../Library/include/Network.hpp"

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

#define DEFAULT_PORT "27015"
#define DEFAULT_BUFLEN 512

SOCKET ConnectSocket;
uqac::network::Network myClient;

void callback(char* message, int iResult, int bufflen) {
	if (iResult == SOCKET_ERROR) {
		std::cout << "In use case, send failed : " << WSAGetLastError() << std::endl;
		myClient.myConnection->shutdownAllConnections();
		WSACleanup();
		return;
	}
}

void callback2(char* message, int iResult, int source){
	std::string messageRcv(message, message + iResult);
	std::cout << "[Client] Received: " << messageRcv << std::endl;
}

int main(int argc, char **argv) {

	myClient = uqac::network::Network(STR_TCP_PROTOCOL);
	std::function recvCallback = callback2;

	if (argc > 1) {
		std::cout << "[Client] Given address: " << argv[1] << std::endl;
		myClient.createClient(callback2, argv[1]);
	}
	else {
		std::cout << "[Client] No arguments, using localhost" << std::endl;
		myClient.createClient(callback2);
	}

		char sendbuf[DEFAULT_BUFLEN];

		int iResult = 0;

		bool askForExit = false;

		while (!askForExit) {
			std::cout << "[Client] Enter 'quit' to disconnect. Please, enter your message : " << std::endl;
			std::cin.getline(sendbuf, DEFAULT_BUFLEN);

			// If message match with EXIT_CMD 
			if (myClient.isExitCmd(sendbuf)) {
				askForExit = true;
			}
			// Client does not want to quit
			else {
				std::function myCallback = callback;
				iResult = myClient.myConnection->sendMessage(sendbuf, (int)strlen(sendbuf), myCallback);
			}
			
		}

	myClient.disconnect();

	std::cout << "Press enter to exit" << std::endl;
	std::cin.get(); // wait for enter (ie for a complete line)
}

