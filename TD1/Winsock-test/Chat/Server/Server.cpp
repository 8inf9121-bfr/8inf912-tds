#include <iostream>
#include "../../Library/include/Network.hpp"

//using uqac::network;
#define DEFAULT_PORT "27015"

void emptyCallback(char* buf, int buflen, int sendbuflen) {
	//std::cout << "[Server] Sent " << sendbuflen << " out of " << buflen << "bytes" << std::endl;
}

std::function<void (char*, int, int)> createCallback(uqac::network::Network server) {
	return [=](char* recvbuf, int iResult, int sourceSocket) {
		if (iResult > 0) {
			std::string message(recvbuf, recvbuf + iResult);
			std::cout << "[Server] Received: " << message << " from socket " << sourceSocket << std::endl;

			std::function<void(char*, int, int)> myCallback = emptyCallback;

			int iSendResult = server.myConnection->broadcast(recvbuf, iResult, sourceSocket,myCallback);
			std::cout << "[Server] Broadcasted message from " << sourceSocket << std::endl;
		}
		else if (iResult == 0)
			std::cout << "[Server] Connection closing..." << std::endl;
	};
}

int main() {

	uqac::network::Network myServer = uqac::network::Network(STR_TCP_PROTOCOL);

	char recvbuf[DEFAULT_BUFLEN];
	int iResult, iSendResult;
	int recvbuflen = DEFAULT_BUFLEN;

	// Create callback
	std::function<void(char*, int, int)> myCallback = createCallback(myServer);

	myServer.createServer(myCallback, DEFAULT_PORT);
	for (;;) {

	}

	myServer.shutdownServer();

	std::cout << "[Server] Press enter to exit" << std::endl;
	std::cin.get(); // wait for enter (ie for a complete line)
}
