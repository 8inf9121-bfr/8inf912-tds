#include <string>
#include <cstdlib>
#include <ctime>
#include <Framework.h>
#include <Compression.hpp>
#include <Decompression.hpp>

std::pair<float, float> positionXRange(-500.f, 500.f);
std::pair<float, float> positionYRange(-500.f, 500.f);
std::pair<float, float> positionZRange(0.f, 100.f);
std::pair<float, float> quaternionRange(-1.f, 1.f);
std::pair<int, int> lifeRange(0, 300);
std::pair<int, int> armorRange(0, 50);
int defaultPrecision = 3;
std::pair<float, float> moneyRange(-99999.99f, 99999.99f);
int moneyPrecision = 2;

float generateRandomFloat(float min, float max) {
	return min + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX/(max - min)));
}

int generateRandomInt(int min, int max) {
	return min + rand() % (max - min);
}

std::string generateRandomString(int maxLength) {
	std::string str;
	static const char alphanum[] =
		"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!#$%&'()*+,-./:;<=>?@[]^_`{|}~";
	int length = generateRandomInt(0, maxLength - 1);

	for (int i = 0; i < length; ++i) {
		str += alphanum[rand() % (sizeof(alphanum) - 1)];
	}

	return str;
}

void compress(uqac::compression::Compression* compressor, std::string name, int life, int armor, float money,
	uqac::utils::Vector3<float> position, uqac::utils::Vector3<float> size,
	uqac::utils::Quaternion rotation) {

	compressor->compressString(name.c_str());
	compressor->compressIntWithBytePrecision(life, lifeRange);
	compressor->compressIntWithBytePrecision(armor, armorRange);
	compressor->compressFloatWithBytePrecision(money, moneyRange, moneyPrecision);
	compressor->compressVector3Float(position, positionXRange, positionYRange, positionZRange, defaultPrecision);
	compressor->compressVector3Float(size, positionXRange, positionYRange, positionZRange, defaultPrecision);
	compressor->compressQuaternion(rotation, defaultPrecision);
}

void decompress(uqac::compression::Decompression* decompressor, std::string name, int life, int armor, float money,
	uqac::utils::Vector3<float> position, uqac::utils::Vector3<float> size,
	uqac::utils::Quaternion rotation) {

	char compName[128] = {};
	decompressor->decompressString(compName);
	std::cout << "\tName :" << std::endl << name << std::endl << " -> " << std::endl << compName << std::endl;

	int compLife = decompressor->decompressIntWithBytePrecision(lifeRange);
	std::cout << "\tLife :" << std::endl << life << " -> " << compLife << std::endl;

	int compArmor = decompressor->decompressIntWithBytePrecision(armorRange);
	std::cout << "\tArmor :" << std::endl << armor << " -> " << compArmor << std::endl;

	float compMoney = decompressor->decompressFloatWithBytePrecision(moneyRange, moneyPrecision);
	std::cout << "\tMoney :" << std::endl << money << " -> " << compMoney << std::endl;

	uqac::utils::Vector3<float> compPosition = decompressor->decompressVector3Float(positionXRange, positionYRange, positionZRange, defaultPrecision);
	std::cout << "\tPosition :" << std::endl << position << std::endl << " -> " << std::endl << compPosition << std::endl;

	uqac::utils::Vector3<float> compSize = decompressor->decompressVector3Float(positionXRange, positionYRange, positionZRange, defaultPrecision);
	std::cout << "\tSize :" << std::endl << size << std::endl << " -> " << std::endl << compSize << std::endl;

	uqac::utils::Quaternion compRotation = decompressor->decompressQuaternion(defaultPrecision);
	std::cout << "\tRotation :" << std::endl << rotation << std::endl << " -> " << std::endl << compRotation << std::endl;
}

void testPlayer(std::string name, int life, int armor, float money,
	uqac::utils::Vector3<float> position, uqac::utils::Vector3<float> size,
	uqac::utils::Quaternion rotation) {
	/* COMPRESSION */

	uqac::compression::Compression compressor;
	compress(&compressor, name, life, armor, money, position, size, rotation);

	/* DECOMPRESSION */

	std::string buffer = compressor.getSerializerContent();
	uqac::compression::Decompression decompressor(buffer.c_str(), buffer.size());
	decompress(&decompressor, name, life, armor, money, position, size, rotation);
}

void testPlayerDataMax() {
	uqac::utils::Vector3<float> position(positionXRange.second, positionYRange.second, positionZRange.second);
	uqac::utils::Quaternion rotation(0.707f, 0.707f, 0.f, 0.f);
	uqac::utils::Vector3<float> size(positionXRange.second, positionYRange.second, positionZRange.second);
	int life = lifeRange.second;
	int armor = armorRange.second;
	float money = moneyRange.second;
	std::string name("SXpds9mmuW3Tdbh9JjiyjU7t3cqm2OGiHuf6dU7QnSwewazetKXznj34gu4jZ6GaUC2sDck2pTDSNQkFkhA23g1w7Rlh1w3sCeCe47zaeBfUAqXcm0CVOtQi3qpoCXD");

	std::cout << "\t\tTEST PLAYER DATA MAX BEGIN" << std::endl;
	testPlayer(name, life, armor, money, position, size, rotation);
	std::cout << "\t\tTEST PLAYER DATA MAX END" << std::endl;
}

void testPlayerDataMin() {
	uqac::utils::Vector3<float> position(positionXRange.first, positionYRange.first, positionZRange.first);
	uqac::utils::Quaternion rotation(-0.707f, -0.707f, 0.f, 0.f);
	uqac::utils::Vector3<float> size(positionXRange.first, positionYRange.first, positionZRange.first);
	int life = lifeRange.first;
	int armor = armorRange.first;
	float money = moneyRange.first;
	std::string name("");

	std::cout << "\t\tTEST PLAYER DATA MIN BEGIN" << std::endl;
	testPlayer(name, life, armor, money, position, size, rotation);
	std::cout << "\t\tTEST PLAYER DATA MIN END" << std::endl;
}

void testPlayerDataNull() {
	uqac::utils::Vector3<float> position(0.f, 0.f, 0.f);
	uqac::utils::Quaternion rotation(1.f, 0.f, 0.f, 0.f);
	uqac::utils::Vector3<float> size(0.f, 0.f, 0.f);
	int life = 0;
	int armor = 0;
	float money = 0.f;
	std::string name("");

	std::cout << "\t\tTEST PLAYER DATA NULL BEGIN" << std::endl;
	testPlayer(name, life, armor, money, position, size, rotation);
	std::cout << "\t\tTEST PLAYER DATA NULL END" << std::endl;
}

void testPlayerDataRand() {
	uqac::utils::Vector3<float> position(
		generateRandomFloat(positionXRange.first, positionXRange.second),
		generateRandomFloat(positionYRange.first, positionXRange.second),
		generateRandomFloat(positionZRange.first, positionXRange.second)
	);
	uqac::utils::Quaternion rotation(
		0.632f,
		0.548f,
		0.447f,
		0.316f
	);
	uqac::utils::Vector3<float> size(
		generateRandomFloat(positionXRange.first, positionXRange.second),
		generateRandomFloat(positionYRange.first, positionXRange.second),
		generateRandomFloat(positionZRange.first, positionXRange.second)
	);
	int life = generateRandomInt(lifeRange.first, lifeRange.second);
	int armor = generateRandomInt(armorRange.first, armorRange.second);
	float money = generateRandomFloat(moneyRange.first, moneyRange.second);
	std::string name(generateRandomString(128));

	std::cout << "\t\tTEST PLAYER DATA RAND BEGIN" << std::endl;
	testPlayer(name, life, armor, money, position, size, rotation);
	std::cout << "\t\tTEST PLAYER DATA RAND END" << std::endl;
}

int main()
{
	std::cout.flags(std::cout.fixed);
	std::cout.precision(defaultPrecision);
	srand(static_cast<unsigned>(time(0)));

	testPlayerDataMax();
	testPlayerDataMin();
	testPlayerDataNull();
	testPlayerDataRand();

	return 0;
}